#include <iostream>
#include <string>
#include "rpc/proto/user.pb.h"
#include "rpc/rpcprovider.h"


// RPC服务提供者
class UserService : public sylar::UserServiceRpc {
public:
    virtual void Login(::google::protobuf::RpcController *controller,
                       const ::sylar::LogInRequest *request,
                       ::sylar::LogInResponse *response,
                       ::google::protobuf::Closure *done) {
        auto name = request->name();
        auto pwd = request->pwd();
        bool loginRes = Login(name, pwd);
        sylar::ResultCode *code = response->mutable_result();
        code->set_errcode(1);
        code->set_errmsg("login fail lalalalla");
        response->set_success(loginRes);
        done->Run();
    }

    bool Login(const std::string &name, const std::string &pwd) {
        std::cout << " Login Method " << std::endl;
        std::cout << " name = " << name << " pwd = " << pwd << std::endl;
        return true;
    }
};

void run() {
    // 框架初始化操作 配置文件、日志类初始化的操作
    // 注册一个RPC方法
    std::shared_ptr<sylar::RpcProvider> provider(new sylar::RpcProvider());
    auto addr =  sylar::Address::LookupAny("0.0.0.0:8020");
    provider->bind(addr);
    provider->NotifyService(new UserService());
    // 启动一个Rpc服务发现节点
    provider->Run();
}

int main(int argc, char **argv) {
    sylar::IOManager iom(2);
    iom.schedule(run);
    return 0;
}
