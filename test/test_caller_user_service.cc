#include <iostream>
#include "rpc/proto/user.pb.h"
#include "rpc/rpchannel.h"


int main(int argc, char **argv) {
    sylar::UserServiceRpc_Stub stub(new sylar::MprpcChannel());
    sylar::LogInRequest request;
    request.set_name("zhang san");
    request.set_pwd("123456");
    sylar::LogInResponse response;
    stub.Login(nullptr, &request, &response, nullptr);

    if (response.result().errcode() == 0) {
        // success
        std::cout << "rpc login response success: " << response.success() << std::endl;
    } else {
        // fail
        std::cout << "rpc login response error: " << response.result().errmsg() << std::endl;
    }

    exit(0);
}
