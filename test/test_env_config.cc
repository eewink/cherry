#include "config.h"
#include "log.h"
#include <yaml-cpp/yaml.h>
#include "env.h"
#include <iostream>

void test_loadconf() {
    sylar::Config::LoadFromConfDir("conf");
}

int main(int argc, char** argv) {
    sylar::EnvMgr::GetInstance()->init(argc, argv);
    test_loadconf();
    std::cout << " ==== " << std::endl;
    sleep(10);
    test_loadconf();
    return 0;

}