### 模块功能说明

- 日志模块
    - 支持不同级别的日志打印
    ```c++
     enum Level {
            /// 未知级别
            UNKNOW = 0,
            /// DEBUG 级别
            DEBUG = 1,
            /// INFO 级别
            INFO = 2,
            /// WARN 级别
            WARN = 3,
            /// ERROR 级别
            ERROR = 4,
            /// FATAL 级别
            FATAL = 5
        };
    ```
    - 输出可定向到标准输出或者文件流
- 配置模块
    - 支持内部或者外部配置的导入
- 线程模块
    - 支持信号量与多种锁
  ```c++
  class Semaphore;
  class Mutex;
  class RWMutex;
  class Spinlock;
  class CASLock;
  ```
  - 线程池：每个线程执行`cb()`
- 协程模块
  - 基于`ucontext`
  - 流程
  ```c++
  Fiber::GetScheduler(); -> 第一次调用将会创建t_threadFiber(fiber_id = 0),指定当前运行的协程t_fiber = t_threadFiber
  Fiber::ptr fiber(new Fiber(cb)); -> 通过callback function构建新的fiber
  ```
- 协程调度模块
  - 1 scheduler -> N threads -> M fibers
  - 流程
  ```c++
  1. 初始化
        Scheduler(size_t threads, bool use_caller, const std::string& name);
                                  |
                                  v
        use_caller is true : 将协程调度器所在的线程纳入到线程池中
              Fiber::GetScheduler(); -> 创建t_threadFiber(id = 0),当前线程运行的协程t_fiber = t_threadFiber
              线程池线程数量减1
              t_scheduler = this;-> 指定全局唯一调度器
              this.m_rootFiber.reset(new Fiber(std::bind(&Scheduler::Run, this), 0, true)); -> 创建协程调度器需要运行的cb
              t_scheduler_run_fiber = m_rootFiber.get(); -> 指定调度器协程
              m_rootThread = cherry::GetThreadId(); -> 指定调度器协程所在线程的id
        use_caller is false : 将协程调度器所在的线程不纳入到线程池中
              m_rootThread = -1; -> 表示没有将协程调度器所在的线程纳入到线程池中
  2. start
        创建线程
        m_threads[i].reset(new Thread(std::bind(&Scheduler::Run, this) , m_name + "_" + std::to_string(i)));
        
        2.1 Scheduler::run函数
            指定协程调度器 <-通过std::bind获得到的Scheduler的this指针得到
            如果GetThreadId() != m_rootThread 需要创建t_threadFiber(id = 0),当前线程运行的协程t_fiber = t_threadFiber
            并且指定 t_scheduler_run_fiber = t_threadFiber = t_cur_run_fiber(id = 0)
            寻找cb 并在t_scheduler_fiber与此fiber之间切换
  ```
- 定时器模块
- io管理模块
- hook模块
  - 使用`dlsym`更换默认`send` `write` `sleep`等函数
- fd管理器
- 字节流
- stream流
- 序列化
- http协议封装
  - `HTTP/1.1`
  - `HttpRequest`
  - `HttpResponse`
  - `http.h` / `http.c` -> http协议
    - `enum class HttpMethod`
    - `enum class HttpStatus `
    - `class HttpRequest`
    - `class HttpResponse`
  - 请求 响应解析 `mongrel2 使用有限状态机编写的http高效率解析` ->http报文->HttpRequest/HttpResponse
  - `http_common.h`
  - `http11_parser.h` /  `http11_parser.cc` ->解析请求
  - `http11_client_parser.h` /  `http11_client_parser.cc` ->解析响应
  - ```shell
     ragel -C httpclient_parser.rl -o httpclient_parser.cc
     ```
  - `http_parser.h` 基于mongrel2实现的http报文->内置自定义类`HttpRequest` `HttpResponse`
- `tcp_server`
- `stream`流的封装
- `HttpSession`/`HttpConntecion`
  - `server.accept, socket -> session`
  - `client connect socket -> connection`
  - `HttpSession`
    - `HttpServer`
    - `HttpServlet`
  - `HttpConntecion`
  