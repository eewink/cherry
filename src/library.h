//
// Created by zwh on 7/9/24.
//

#ifndef CHERRY_LIBRARY_H
#define CHERRY_LIBRARY_H


#include <memory>
#include "module.h"

namespace sylar {

    class Library {
    public:
        static Module::ptr GetModule(const std::string &path);
    };

}


#endif //CHERRY_LIBRARY_H
