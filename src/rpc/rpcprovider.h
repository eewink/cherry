//
// Created by zwh on 24-7-10.
//

#ifndef CHERRY_RPCPROVIDER_H
#define CHERRY_RPCPROVIDER_H

#include <google/protobuf/service.h>
#include <memory>
#include "socket.h"
#include "tcp_server.h"

namespace sylar {
    class RpcProvider : public sylar::TcpServer {

    public:
        RpcProvider();

        // 发布rpc方法
        void NotifyService(google::protobuf::Service *service);

        //启动rpc服务节点
        void Run();

        void handleClient(Socket::ptr client) override;

        void SendRpcResponse(Socket::ptr client, google::protobuf::Message *response);

    private:
        struct ServiceInfo {
            google::protobuf::Service *m_service;//服务对象
            std::unordered_map<std::string, const google::protobuf::MethodDescriptor *> m_methodMap;//服务方法
        };
        // 存储注册成功的服务对象和其服务方法的所有info
        std::unordered_map<std::string, ServiceInfo> m_serviceMap;
    };
}


#endif //CHERRY_RPCPROVIDER_H
