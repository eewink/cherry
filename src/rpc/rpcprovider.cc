//
// Created by zwh on 24-7-10.
//

#include "rpcprovider.h"
#include "proto/rpcheader.pb.h"
#include "zookeeperwarp.h"
#include "log.h"
#include "config.h"
#include "bytearray.h"

namespace sylar {
    static Logger::ptr g_logger = SYLAR_LOG_NAME("system");

    RpcProvider::RpcProvider() {
    }

    void RpcProvider::NotifyService(google::protobuf::Service *service) {
        ServiceInfo serviceInfo;
        //获取描述信息
        const google::protobuf::ServiceDescriptor *pserviceDesc = service->GetDescriptor();
        // "UserServiceRpc"
        std::string servicename = pserviceDesc->name();
        SYLAR_LOG_INFO(g_logger) << "NotifyService : servicename is " << servicename << std::endl;
        int methodCnt = pserviceDesc->method_count();
        for (int i = 0; i < methodCnt; ++i) {
            auto MethodDescriptor = pserviceDesc->method(i);
            std::string method_name = MethodDescriptor->name();
            SYLAR_LOG_INFO(g_logger) << "servicename's  method " << i << " is " << method_name << std::endl;
            serviceInfo.m_methodMap.insert({method_name, MethodDescriptor});
        }
        serviceInfo.m_service = service;
        m_serviceMap.insert({servicename, serviceInfo});
    }

    void RpcProvider::Run() {
        SYLAR_LOG_INFO(g_logger) << "RpcService start at ip = "
                                 << Config::Lookup<std::string>("zookeeper.ip")->getValue() << ",port = "
                                 << Config::Lookup<std::string>("zookeeper.port")->getValue();

        ZkClient zkCli;
        zkCli.start();
        // servicename为永久节点 methodname为临时节点
        for (auto &p: m_serviceMap) {
            std::string service_path = "/" + p.first;
            zkCli.create(service_path.c_str(), nullptr, 0);
            for (auto &map: p.second.m_methodMap) {
                std::string method_path = service_path + "/" + map.first;
                std::string ip_port =  Config::Lookup<std::string>("zookeeper.ip")->getValue() + ":"+Config::Lookup<std::string>("zookeeper.port")->getValue();
                zkCli.create(method_path.c_str(), ip_port.c_str(),ip_port.size() , ZOO_EPHEMERAL);
            }
        }
        this->start();
    }

    void RpcProvider::handleClient(Socket::ptr client) {
        SYLAR_LOG_INFO(g_logger) << " RpcProvider handleClient " << *client;
        sylar::ByteArray::ptr ba(new sylar::ByteArray);
        while (true) {
            ba->clear();
            std::vector<iovec> iovs;
            ba->getWriteBuffers(iovs, 1024);

            int rt = client->recv(&iovs[0], iovs.size());
            if (rt == 0) {
                SYLAR_LOG_INFO(g_logger) << "client close: " << *client;
                break;
            } else if (rt < 0) {
                SYLAR_LOG_INFO(g_logger) << "client error rt=" << rt
                                         << " errno=" << errno << " errstr=" << strerror(errno);
                break;
            }
            ba->setPosition(ba->getPosition() + rt);
            ba->setPosition(0);

            std::string recv_buf = ba->toString();
            uint32_t header_size = 0;
            recv_buf.copy((char *) &header_size, 4, 0);
            std::string rpc_header = recv_buf.substr(4, header_size);
            sylar::RpcHeader rpcHeader;
            std::string service_name;
            std::string method_name;
            uint32_t arg_size;
            if (rpcHeader.ParseFromString(rpc_header)) {
                //success
                service_name = rpcHeader.service_name();
                method_name = rpcHeader.method_name();
                arg_size = rpcHeader.args_size();

            } else {
                SYLAR_LOG_ERROR(g_logger) << "rpc_header : " << rpc_header << " parse error!";
                return;
            }

            std::string args_str = recv_buf.substr(4 + header_size, arg_size);

            SYLAR_LOG_INFO(g_logger) << "header_size :" << header_size;
            SYLAR_LOG_INFO(g_logger) << "rpc_header :" << rpc_header;
            SYLAR_LOG_INFO(g_logger) << "service_name :" << service_name;
            SYLAR_LOG_INFO(g_logger) << "method_name :" << method_name;
            SYLAR_LOG_INFO(g_logger) << "args_str :" << args_str;

            // 获取service和method
            auto it = m_serviceMap.find(service_name);
            if (it == m_serviceMap.end()) {
                std::cout << "service_name :" << service_name << "not find" << std::endl;
                return;
            }

            auto itm = it->second.m_methodMap.find(method_name);
            if (itm == it->second.m_methodMap.end()) {
                std::cout << "method_name :" << method_name << "not find" << std::endl;
                return;
            }

            google::protobuf::Service *service = it->second.m_service;
            const google::protobuf::MethodDescriptor *method = itm->second;

            // 生成request
            // 获取servide中某一个method方法的请求类型与响应类型
            google::protobuf::Message *request = service->GetRequestPrototype(method).New();
            if (!request->ParseFromString(args_str)) {
                std::cout << "request parse error content: " << args_str << std::endl;
            }
            // 给method绑定一个回调
            google::protobuf::Message *response = service->GetResponsePrototype(method).New();
            google::protobuf::Closure *done = google::protobuf::NewCallback<RpcProvider, Socket::ptr, google::protobuf::Message *>(
                    this, &RpcProvider::SendRpcResponse, client, response);
            service->CallMethod(method, nullptr, request, response, done);
            break;
        }
        client->close();
    }

    void RpcProvider::SendRpcResponse(Socket::ptr client, google::protobuf::Message *response) {
        std::string response_str;
        if (response->SerializeToString(&response_str)) {
            client->send(response_str.c_str(), response_str.size());
        } else {
            SYLAR_LOG_ERROR(g_logger) << "serialize response_str error!";
        }
    }

}
