//
// Created by zwh on 24-7-10.
//

#include "rpchannel.h"
#include "proto/rpcheader.pb.h"
#include "zookeeperwarp.h"
#include <google/protobuf/descriptor.h>
#include <google/protobuf/message.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


namespace sylar{
    void
    MprpcChannel::CallMethod(const google::protobuf::MethodDescriptor *method, google::protobuf::RpcController *controller,
                             const google::protobuf::Message *request, google::protobuf::Message *response,
                             google::protobuf::Closure *done) {
        const google::protobuf::ServiceDescriptor *sd = method->service();
        std::string service_name = sd->name();
        std::string method_name = method->name();

        // 获取参数的序列化字符串
        std::string args_str;
        size_t arg_size = 0;
        if (request->SerializeToString(&args_str)) {
            arg_size = args_str.size();
        } else {
            std::cout << " seroaize error" << std::endl;
            return;
        }
        sylar::RpcHeader rpcHeader;
        rpcHeader.set_service_name(service_name);
        rpcHeader.set_method_name(method_name);
        rpcHeader.set_args_size(arg_size);
        uint32_t header_size = 0;
        std::string header_str;
        rpcHeader.SerializeToString(&header_str);
        header_size = header_str.size();

        std::string send_rpc_str(4, '\0');
        memcpy(&send_rpc_str[0], &header_size, sizeof(header_size));
        send_rpc_str += header_str;
        send_rpc_str += args_str;

        int client_fd = socket(AF_INET, SOCK_STREAM, 0);
        if (client_fd == -1) {
            std::cout << "create sock fail " << errno << std::endl;
            exit(EXIT_FAILURE);
        }

        ZkClient zkCli;
        zkCli.start();
        std::string method_path = "/" + service_name + "/" + method_name;
        std::string host_data = zkCli.getdata(method_path.c_str());
        int idx = host_data.find(":");
        std::string ip = host_data.substr(0, idx);
        uint16_t port = atoi(host_data.substr(idx + 1, host_data.size() - idx).c_str());

//        ip =  "127.0.0.1";
//        port = 8020;

        sockaddr_in servaddr;
        servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(port);
        servaddr.sin_addr.s_addr = inet_addr(ip.c_str());
        if (connect(client_fd, (sockaddr *) &servaddr, sizeof(servaddr)) == -1) {
            std::cout << "connect fail " << errno << std::endl;
            exit(EXIT_FAILURE);
        }
        if (send(client_fd, send_rpc_str.c_str(), send_rpc_str.size(), 0) == -1) {
            std::cout << "send fail " << errno << std::endl;
        }

        char recv_buf[1024] = {0};
        int recv_size = 0;
        if ((recv_size = recv(client_fd, recv_buf, 1024, 0)) == -1) {
            std::cout << "recv fail " << errno << std::endl;
            close(client_fd);
            return;
        }
//    std::string response_str(recv_buf, 0, recv_size);
        response->ParseFromArray(recv_buf, recv_size);
        close(client_fd);
    }

}