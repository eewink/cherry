//
// Created by zwh on 24-7-10.
//

#include "zookeeperwarp.h"
#include "config.h"
#include <iostream>

namespace sylar {

    static sylar::ConfigVar<std::string>::ptr g_zookeeper_ip
            = Config::Lookup("zookeeper.ip", std::string("127.0.0.1"), "zookeeper ip");
    static sylar::ConfigVar<std::string>::ptr g_zookeeper_port
            = Config::Lookup("zookeeper.port", std::string("2181"), "zookeeper port");

    void global_watcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx) {
        if (type == ZOO_SESSION_EVENT) {
            if (state == ZOO_CONNECTED_STATE) {
                // 指定句柄获取sem
                sem_t *sem = (sem_t *) zoo_get_context(zh);
                sem_post(sem);
            }
        }
    }


    ZkClient::ZkClient() : m_zhandle(nullptr) {

    }

    ZkClient::~ZkClient() {
        if (m_zhandle) {
            zookeeper_close(m_zhandle);
        }
    }

    void ZkClient::start() {
        std::string host = g_zookeeper_ip->getValue();
        std::string port = g_zookeeper_port->getValue();
        std::string connstr = host + ":" + port;
        m_zhandle = zookeeper_init(connstr.c_str(), global_watcher, 30000, nullptr, nullptr, 0);
        if (m_zhandle == nullptr) {
            std::cout << "zookeeper init fail\n";
            exit(EXIT_FAILURE);
        }
        sem_t sem;
        sem_init(&sem, 0, 0);
        zoo_set_context(m_zhandle, &sem);
        sem_wait(&sem);
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT()) << "zookeeper success";
    }

    void ZkClient::create(const char *path, const char *data, int data_len, int state) {
        char path_buf[1024];
        int buffer_len = 1024;
        int flag = zoo_create(m_zhandle, path, data, data_len, &ZOO_OPEN_ACL_UNSAFE, state, path_buf, buffer_len);
        if (ZNONODE == flag) {
            std::cout << "znode create success path = " << path << std::endl;
        } else {
            std::cout << "flag: " << flag << std::endl;
            std::cout << "znode create fail path = " << path << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    std::string ZkClient::getdata(const char *path) {
        char buf[128];
        int buff_len = 128;
        int flag = zoo_get(m_zhandle, path, 0, buf, &buff_len, nullptr);
        if (flag != ZOK) {
            std::cout << "GetData fail path = " << path << std::endl;
            return "";
        } else {
            return buf;
        }

    }

}