//
// Created by zwh on 24-7-10.
//

#ifndef CHERRY_RPCHANNEL_H
#define CHERRY_RPCHANNEL_H


#include <google/protobuf/service.h>

namespace sylar{
    class MprpcChannel : public google::protobuf::RpcChannel {
    public:
        // 所有通过stub代理调用的Rpc方法都走到这里，做序列化与网络发送
        void CallMethod(const google::protobuf::MethodDescriptor *method,
                        google::protobuf::RpcController *controller, const google::protobuf::Message *request,
                        google::protobuf::Message *response, google::protobuf::Closure *done);

    private:
    };
}



#endif //CHERRY_RPCHANNEL_H
