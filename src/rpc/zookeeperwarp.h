//
// Created by zwh on 24-7-10.
//

#ifndef CHERRY_ZOOKEEPERWARP_H
#define CHERRY_ZOOKEEPERWARP_H

#include <semaphore.h>
#include <zookeeper/zookeeper.h>
#include <string>

namespace sylar {
    class ZkClient {
    public:
        ZkClient();

        ~ZkClient();

        void start();

        void create(const char *path, const char *data, int data_len, int state = 0);

        std::string getdata(const char *path);

    private:
        zhandle_t *m_zhandle;
    };
}


#endif //CHERRY_ZOOKEEPERWARP_H
