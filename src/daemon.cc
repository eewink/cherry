//
// Created by zwh on 24-7-8.
//

#include "daemon.h"
#include "log.h"
#include "config.h"
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

namespace sylar {
    static sylar::Logger::ptr g_logger = SYLAR_LOG_NAME("system");
    static sylar::ConfigVar<uint32_t>::ptr g_daemon_restart_interval
            = sylar::Config::Lookup("daemon.restart_interval", (uint32_t) 5, "daemon restart interval");

    std::string ProcessInfo::toString() const {
        std::stringstream ss;
        ss << "[ProcessInfo parent_id=" << parent_id
           << " main_id=" << main_id
           << " parent_start_time=" << sylar::Time2Str(parent_start_time)
           << " main_start_time=" << sylar::Time2Str(main_start_time)
           << " restart_count=" << restart_count << "]";
        return ss.str();
    }

    static int real_start(int argc, char **argv,
                          std::function<int(int argc, char **argv)> main_cb) {
        return main_cb(argc, argv);
    }

    static int real_daemon(int argc, char **argv,
                           std::function<int(int argc, char **argv)> main_cb) {
        // 将当前进程转变为守护进程
        // 第一个参数为1表示在创建守护进程时，不改变当前的工作目录
        // 第二个参数为0表示在创建守护进程时，不关闭文件描述符
        daemon(1, 0);
        // 进程ID和启动时间记录到ProcessInfoMgr中
        ProcessInfoMgr::GetInstance()->parent_id = getpid();
        ProcessInfoMgr::GetInstance()->parent_start_time = time(0);
        while (true) {
            pid_t pid = fork();
            if (pid == 0) {
                // 将自己的进程ID和启动时间记录到ProcessInfoMgr中，然后调用real_start()函数开始执行真正的任务
                ProcessInfoMgr::GetInstance()->main_id = getpid();
                ProcessInfoMgr::GetInstance()->main_start_time = time(0);
                SYLAR_LOG_INFO(g_logger) << "process start pid=" << getpid();
                return real_start(argc, argv, main_cb);
            } else if (pid < 0) {
                SYLAR_LOG_ERROR(g_logger) << "fork fail return=" << pid
                                          << " errno=" << errno << " errstr=" << strerror(errno);
                return -1;
            } else {
                // 父进程返回
                int status = 0;
                // 等待子进程的结束
                waitpid(pid, &status, 0);
                if (status) {
                    // 父进程就在一段时间后重启子进程
                    SYLAR_LOG_ERROR(g_logger) << "child crash pid=" << pid
                                              << " status=" << status;
                } else {
                    // 子进程正常结束，父进程就退出循环
                    SYLAR_LOG_INFO(g_logger) << "child finished pid=" << pid;
                    break;
                }
                ProcessInfoMgr::GetInstance()->restart_count += 1;
                sleep(g_daemon_restart_interval->getValue());
            }
        }
        return 0;
    }

    int start_daemon(int argc, char **argv, std::function<int(int, char **)> main_cb, bool is_daemon) {
        if (!is_daemon) {
            return real_start(argc, argv, main_cb);
        }
        return real_daemon(argc, argv, main_cb);
    }

}